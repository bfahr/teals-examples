"""
Unit 6 Project - Song Database
==============================

In this project you will be reading data from a string to start your database.
The data is in CSV (Comma Separated Values) format [1]. This means that each line of the
string will contain an artist name, a song title, and an album title, separated by commas.
The provided function read_song_db read that data into a list. Each line
of the list will look like this:

    "Michael Jackson,Billie Jean,Thriller"

The first line of the file usually contains the column names, in this case that would be

    "artist,song title,album"

Spreadsheet programs can read and write CSV files, and it is a common format for data
provided on the internet.

[1] https://en.wikipedia.org/wiki/Comma-separated_values

Part 1
------

Implement the load_initial_db function to split each line of data to get artist, song and album
and save the information in a database that is a dictionary.  You may structure the dictionary
any way you choose. Read through the rest of the Project description to make sure you understand
how you will be accessing the data.  That should influence how you structure your dictionary
database.

Part 2
------

Add the following capabilities using the template functions provided in the sample code to your
game loop:

    - Print the commands (help)
    - Print the information in the database
    - Print the artists names
    - Print the album names
    - Find all songs by an artist name (find_artist_songs)
    - Find a song title that contains specified text (find_songs_like)
    - Quit the program

Part 3
------

    - Add the capability to add a new song to the database.
    - Add the capability to remove a song from the database.

Part 4
------

Add the ability to save your song database using the backup_db function.  This will
save the song database dictionary into a file with the name "song_db.json". JavaScript Object
Notation (JSON) [2] is a lightweight data interchange format and built into Python.  JSON files
can be read by most other programming languages as well as many tools, including web browsers.
Open the file manager in PythonAnywhere and open this file after you have backed it up.

Once you have backed up your song database, use the restore_db function to restore the song
database dictionary.  Print all the records and make sure the object has the same information.

[2] https://www.json.org/json-en.html

Bonus
-----

Add functionality to modify a song to add a genre [3] and a song rating.  Genre is a category
that classifies a song based on various criteria.  Examples of genres include "rock", "country",
and "pop", but there are literally hundreds of genres and subgenres. The song rating is typically
a set of stars "*" from 1-5, but you can choose whatever type of rating system you like.

[3] https://en.wikipedia.org/wiki/Music_genre
"""
import json
import os

# Sample starting data for this project
# This is a single string with embedded End-of-Line (EOL) chars at the end of each line
# .strip() at the end of the string removes any extra whitespace at the beginning and end of the string
SAMPLE_CSV_DATA = """
Artist,Song,Album
Taylor Swift,Welcome To New York (Taylor's Version),1989 (Taylor's Version)
Taylor Swift,Blank Space (Taylor's Version),1989 (Taylor's Version)
Peter Frampton,Show Me The Way,Frampton Comes Alive!
Peter Frampton,Do You Feel Like We Do,Frampton Comes Alive!
Michael Jackson,Billie Jean,Thriller
Michael Jackson,Thriller,Thriller
""".strip()


#################################################
# BEGIN: Don't edit this code                   #
#################################################
def read_song_db() -> list:
    """
    Reads CSV data containing a list of artist names and song titles
    in the format:

        artist,song,album

    Where each artist, song and title is separated by a comma, hence the type
    of file is CSV or Comma Separated Values.

    Returns:
        list (str): A list of strings containing artist, song title, and album separated by a comma
    """
    # Define a list to hold the CSV data
    data = []
    # Split a string containing End-of-Line chars (\n) into a list of lines
    rows = SAMPLE_CSV_DATA.splitlines()
    # Get the first line which includes the heading of each column, separated by commas
    # We won't use this data in this application
    headings = rows[0].split(',')

    # Get each row of data, skipping the first row which is headings
    for row in rows[1:]:
        data.append(row)

    return data


# Filename for song DB backup
SONG_DB_FILE = "song_db.json"


def backup_db(song_db: dict) -> None:
    """ Back up the song database to disk """
    with open(SONG_DB_FILE, "w") as file_handle:
        json.dump(song_db, file_handle)


def restore_db() -> dict:
    """ Restore the song database from disk """
    if os.path.exists(SONG_DB_FILE):
        with open(SONG_DB_FILE, "r") as file_handle:
            song_db = json.load(file_handle)
            return song_db
    else:
        print("No song database found, have you backed it up?")
#################################################
# END: Don't edit this code                     #
#################################################


def load_initial_db() -> dict:
    """
    Split each line of data to get artist, song and album and save the information in a database
    that is a dictionary.  You may structure the dictionary any way you choose, but based on the
    functions you will write, some dictionary structures may be more efficient.

    Returns:
        dict: Returns the sample song database as a dictionary
    """
    db = dict()
    # Load the initial song database
    for line in read_song_db():
        # Split line on commas into list
        # song_parts = line.split(',')
        # print("Artist:", song_parts[0], "Song:", song_parts[1], "Album:", song_parts[2])

        # Alternatively split line into tuple
        artist, song_title, album = line.split(',')

        # Switch to use function as this code was in two places
        db = add_song_to_database(db, artist, album, song_title)

    return db


def find_artist_songs(artist: str, in_db: dict) -> list:
    """ Find all songs by an artist name

    Arguments:
        artist (str): The name of the artist to search for in the database
        in_db (dict): Song dictionary to search

    Returns:
        list (str): Returns a list of strings containing the string album/song names or an empty list if
        there are no songs by the artist
    """
    songs = []
    if artist in in_db:
        for album in in_db[artist].keys():
            for song in in_db[artist][album]['songs']:
                songs.append(f"{album}/{song['song_title']}")

    return songs


def find_songs_like(text: str, in_db: dict) -> list:
    """ Find all songs that include ``text`` as part of the song title.

    Arguments:
        text (str): The string to search for in song titles
        in_db (dict): Song dictionary to search

    Returns:
        list (str): A list containing the string artist/album/song for all of matching entries
        or an empty list if there are no songs by the artist
    """
    songs = []
    for artist in in_db.keys():
        for album in in_db[artist].keys():
            for song in in_db[artist][album]['songs']:
                # Ideally I would do the comparison this way to avoid case-sensitive titles
                # if text.lower() == song.lower():
                if text in song['song_title']:
                    songs.append(f"{artist}/{album}/{song['song_title']}")

    return songs


def add_song_to_database(to_db: dict, artist: str, album: str, song: str) -> dict:
    """
    Add a new song to the database if it doesn't already exist

    Note: technically to_db is a reference so any changes to it also change the global
    variable, but it's my preference to return a modified value so that the modification
    isn't hidden.

    Arguments:
        to_db (dict): Song database to add the song to
        artist (str): The name of the artist to add the song to
        album (str): The name of the album to add the song to
        song (str): The name of the song to add to the database

    Returns:
        dict: Song database with the song added to the database
    """
    if artist not in to_db:
        to_db[artist] = {
            album: {'songs': [{'song_title': song, 'rating': ""}, ], 'genre': ""}
        }
    elif album not in to_db[artist]:
        to_db[artist][album] = {'songs': [{'song_title': song, 'rating': ""}, ], 'genre': ""}
    else:
        song_found = False
        for song_info in to_db[artist][album]['songs']:
            if song == song_info['song_title']:
                song_found = True
                break
        if not song_found:
            to_db[artist][album]['songs'].append({'song_title': song, 'rating': ""})
        else:
            # This song is already there
            print(f"Artist {artist} album {album} song {song} is already in the database")

    return to_db


def remove_song_from_database(from_db: dict, artist: str, album: str, song: str) -> dict:
    """
    Remove a song from the database if it exists

    Note: technically from_db is a reference so any changes to it also change the global
    variable, but it's my preference to return a modified value so that the modification
    isn't hidden.

    Arguments:
        from_db (dict): Song database to remove the song from
        artist (str): The name of the artist
        album (str): The name of the album
        song (str): The name of the song

    Returns:
        dict: Song database with the song removed
    """
    if artist in from_db and album in from_db[artist]:
        # Since songs is a list we need to use enumerate to get the index in the list
        # in order to remove it
        for song_index, song_info in enumerate(from_db[artist][album]['songs']):
            if song == song_info['song_title']:
                from_db[artist][album]['songs'].pop(song_index)
                if len(from_db[artist][album]['songs']) == 0:
                    from_db[artist].pop(album)
                    if len(from_db[artist].keys()) == 0:
                        from_db.pop(artist)
                print(f"{artist}/{album}/{song} removed from the database")
                break

    return from_db


def set_song_rating(in_db: dict, artist: str, album: str, song: str, rating: str) -> dict:
    """
    Set the rating for a song in the database.

    Note: technically from_db is a reference so any changes to it also change the global
    variable, but it's my preference to return a modified value so that the modification
    isn't hidden.

    Arguments:
        in_db (dict): Song database:
        artist (str): Name of the artist
        album (str):  Name of the album
        song (str): Name of the song
        rating (str):  The rating of the song

    Returns:
        dict: Updated song database
    """
    if artist in in_db and album in in_db[artist]:
        for song_info in in_db[artist][album]['songs']:
            if song == song_info['song_title']:
                song_info['rating'] = rating
    return in_db


def set_album_genre(in_db: dict, artist: str, album: str, genre: str) -> dict:
    """
    Set the genre for an album

    Note: technically from_db is a reference so any changes to it also change the global
    variable, but it's my preference to return a modified value so that the modification
    isn't hidden.

    Arguments:
        in_db (dict): Song database:
        artist (str): Name of the artist
        album (str):  Name of the album
        genre (str): Genre of the album

    Returns:
        dict: Updated song database
    """
    if artist in in_db and album in in_db[artist]:
        in_db[artist][album]['genre'] = genre

    return in_db


def help():
    """ Print the list of commands supported by the program. """
    print("h - print this help information")
    print("1 - print the database")
    print("2 - print the artist names")
    print("3 - print the album names")
    print("4 - find all songs by an artist name")
    print("5 - find a song title that contains the specified text")
    print("6 - add a new song title to the database")
    print("7 - remove a song title from the database")
    print("8 - backup database")
    print("9 - restore database")
    print("10 - add/change a rating to a song")
    print("11 - add/change an album genre")
    print("q - quit the program")


# Setup program variables
# Set the flag to indicate when to stop
running = True

# Create a new database to hold the songs
song_database = load_initial_db()

# Show help before starting
help()

# Main program loop
while running:
    cmd = input("Enter a command: ")
    if cmd == "q":
        running = False
    elif cmd == "1":
        for db_key in song_database.keys():
            print("Artist:", db_key)
            for album in song_database[db_key].keys():
                print("\tAlbum:", album, ", Genre:", song_database[db_key][album]['genre'])
                for song in song_database[db_key][album]['songs']:
                    print("\t\tSong:", song['song_title'], ", Rating:", song['rating'])
    elif cmd == "2":
        print("Artists:")
        for artist in song_database.keys():
            print(f"\t{artist}")
    elif cmd == "3":
        print("Artists/Albums:")
        for artist in song_database.keys():
            for album in song_database[artist].keys():
                print(f"{artist}/{album}, Genre: {song_database[artist][album]['genre']}")
    elif cmd == "4":
        artist = input("Enter artist name: ")
        songs = find_artist_songs(artist, song_database)
        print(f"Artist: {artist} has {len(songs)} songs")
        for song in songs:
            print(f"\t{song}")
    elif cmd == "5":
        songs_text = input("Enter text to search for in song titles: ")
        songs = find_songs_like(songs_text, song_database)
        print(f"Found {len(songs)} including text {songs_text}")
        for song in songs:
            print(f"\t{song}")
    elif cmd == "6":
        print("Adding song to database")
        artist = input("Enter artist name: ")
        album = input("Enter album name: ")
        song = input("Enter song title: ")
        song_database = add_song_to_database(song_database, artist, album, song)
    elif cmd == "7":
        print("Removing song from database")
        artist = input("Enter artist name: ")
        album = input("Enter album name: ")
        song = input("Enter song title: ")
        song_database = remove_song_from_database(song_database, artist, album, song)
    elif cmd == "8":
        backup_db(song_database)
        print("Database backed up")
    elif cmd == "9":
        song_database = restore_db()
        print("Database restored")
    elif cmd == "10":
        print("Set song rating")
        artist = input("Enter artist name: ")
        album = input("Enter album name: ")
        song = input("Enter song title: ")
        rating = input("Enter the rating of the song (1-4 *'s): ")
        song_database = set_song_rating(song_database, artist, album, song, rating)
    elif cmd == "11":
        print("Set album genre")
        artist = input("Enter artist name: ")
        album = input("Enter album name: ")
        genre = input("Enter album genre: ")
        song_database = set_album_genre(song_database, artist, album, genre)
    elif cmd == "h":
        help()
    else:
        print("You entered an invalid command")
