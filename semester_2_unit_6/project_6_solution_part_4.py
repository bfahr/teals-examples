"""
Unit 6 Project - Song Database
==============================

In this project you will be reading data from a string to start your database.
The data is in CSV (Comma Separated Values) format [1]. This means that each line of the
string will contain an artist name, a song title, and an album title, separated by commas.
The provided function read_song_db read that data into a list. Each line
of the list will look like this:

    "Michael Jackson,Billie Jean,Thriller"

The first line of the file usually contains the column names, in this case that would be

    "artist,song title,album"

Spreadsheet programs can read and write CSV files, and it is a common format for data
provided on the internet.

[1] https://en.wikipedia.org/wiki/Comma-separated_values

Part 1
------

Implement the load_initial_db function to split each line of data to get artist, song and album
and save the information in a database that is a dictionary.  You may structure the dictionary
any way you choose. Read through the rest of the Project description to make sure you understand
how you will be accessing the data.  That should influence how you structure your dictionary
database.

Part 2
------

Add the following capabilities using the template functions provided in the sample code to your
game loop:

    - Print the commands (help)
    - Print the information in the database
    - Print the artists names
    - Print the album names
    - Find all songs by an artist name (find_artist_songs)
    - Find a song title that contains specified text (find_songs_like)
    - Quit the program

Part 3
------

    - Add the capability to add a new song to the database.
    - Add the capability to remove a song from the database.

Part 4
------

Add the ability to save your song database using the backup_db function.  This will
save the song database dictionary into a file with the name "song_db.json". JavaScript Object
Notation (JSON) [2] is a lightweight data interchange format and built into Python.  JSON files
can be read by most other programming languages as well as many tools, including web browsers.
Open the file manager in PythonAnywhere and open this file after you have backed it up.

Once you have backed up your song database, use the restore_db function to restore the song
database dictionary.  Print all the records and make sure the object has the same information.

[2] https://www.json.org/json-en.html

Bonus
-----

Add functionality to modify a song to add a genre [3] and a song rating.  Genre is a category
that classifies a song based on various criteria.  Examples of genres include "rock", "country",
and "pop", but there are literally hundreds of genres and subgenres. The song rating is typically
a set of stars "*" from 1-5, but you can choose whatever type of rating system you like.

[3] https://en.wikipedia.org/wiki/Music_genre
"""
import json
import os

# Sample starting data for this project
# This is a single string with embedded End-of-Line (EOL) chars at the end of each line
# .strip() at the end of the string removes any extra whitespace at the beginning and end of the string
SAMPLE_CSV_DATA = """
Artist,Song,Album
Taylor Swift,Welcome To New York (Taylor's Version),1989 (Taylor's Version)
Taylor Swift,Blank Space (Taylor's Version),1989 (Taylor's Version)
Peter Frampton,Show Me The Way,Frampton Comes Alive!
Peter Frampton,Do You Feel Like We Do,Frampton Comes Alive!
Michael Jackson,Billie Jean,Thriller
Michael Jackson,Thriller,Thriller
""".strip()


#################################################
# BEGIN: Don't edit this code                   #
#################################################
def read_song_db() -> list:
    """
    Reads CSV data containing a list of artist names and song titles
    in the format:

        artist,song,album

    Where each artist, song and title is separated by a comma, hence the type
    of file is CSV or Comma Separated Values.

    Returns:
        list (str): A list of strings containing artist, song title, and album separated by a comma
    """
    # Define a list to hold the CSV data
    data = []
    # Split a string containing End-of-Line chars (\n) into a list of lines
    rows = SAMPLE_CSV_DATA.splitlines()
    # Get the first line which includes the heading of each column, separated by commas
    # We won't use this data in this application
    headings = rows[0].split(',')

    # Get each row of data, skipping the first row which is headings
    for row in rows[1:]:
        data.append(row)

    return data


# Filename for song DB backup
SONG_DB_FILE = "song_db.json"


def backup_db(song_db: dict) -> None:
    """ Back up the song database to disk """
    with open(SONG_DB_FILE, "w") as file_handle:
        json.dump(song_db, file_handle)


def restore_db() -> dict:
    """ Restore the song database from disk """
    if os.path.exists(SONG_DB_FILE):
        with open(SONG_DB_FILE, "r") as file_handle:
            song_db = json.load(file_handle)
            return song_db
    else:
        print("No song database found, have you backed it up?")
#################################################
# END: Don't edit this code                     #
#################################################


def load_initial_db() -> dict:
    """
    Split each line of data to get artist, song and album and save the information in a database
    that is a dictionary.  You may structure the dictionary any way you choose, but based on the
    functions you will write, some dictionary structures may be more efficient.

    Returns:
        dict: Returns the sample song database as a dictionary
    """
    db = dict()
    # Load the initial song database
    for line in read_song_db():
        # Split line on commas into list
        # song_parts = line.split(',')
        # print("Artist:", song_parts[0], "Song:", song_parts[1], "Album:", song_parts[2])

        # Alternatively split line into tuple
        artist, song_title, album = line.split(',')

        # This will take some work by the student to decide how to represent
        # the information in the dictionary.  This is one possible representation
        # but there are others.
        if artist not in db:
            # Initialize this artist with a dictionary using album as key
            # and for each album a list of songs
            db[artist] = {
                album: [song_title, ],
            }
        elif album not in db[artist]:
            # Initialize this album with the first song
            db[artist][album] = [song_title, ]
        elif song_title not in db[artist][album]:
            # Just add this song to the database if it's new
            db[artist][album].append(song_title)
        else:
            # This song is already there
            print(f"Artist {artist} album {album} song {song_title} is already in the database")

    return db


def find_artist_songs(artist: str, in_db: dict) -> list:
    """ Find all songs by an artist name

    Arguments:
        artist (str): The name of the artist to search for in the database
        in_db (dict): Song dictionary to search

    Returns:
        list (str): Returns a list of strings containing the string album/song names or an empty list if
        there are no songs by the artist
    """
    songs = []
    if artist in in_db:
        for album in in_db[artist].keys():
            for song in in_db[artist][album]:
                songs.append(f"{album}/{song}")

    return songs


def find_songs_like(text: str, in_db: dict) -> list:
    """ Find all songs that include ``text`` as part of the song title.

    Arguments:
        text (str): The string to search for in song titles
        in_db (dict): Song dictionary to search

    Returns:
        list (str): A list containing the string artist/album/song for all of matching entries
        or an empty list if there are no songs by the artist
    """
    songs = []
    for artist in in_db.keys():
        for album in in_db[artist].keys():
            for song in in_db[artist][album]:
                # Ideally I would do the comparison this way to avoid case-sensitive titles
                # if text.lower() == song.lower():
                if text in song:
                    songs.append(f"{artist}/{album}/{song}")

    return songs


def help():
    """ Print the list of commands supported by the program. """
    print("h - print this help information")
    print("1 - print the database")
    print("2 - print the artist names")
    print("3 - print the album names")
    print("4 - find all songs by an artist name")
    print("5 - find a song title that contains the specified text")
    print("6 - add a new song title to the database")
    print("7 - remove a song title from the database")
    print("8 - backup database")
    print("9 - restore database")
    print("q - quit the program")


# Setup program variables
# Set the flag to indicate when to stop
running = True

# Create a new database to hold the songs
song_database = load_initial_db()

# Show help before starting
help()

# Main program loop
while running:
    cmd = input("Enter a command: ")
    if cmd == "q":
        running = False
    elif cmd == "1":
        for db_key in song_database.keys():
            print("Artist:", db_key)
            for album in song_database[db_key].keys():
                print("\tAlbum:", album)
                for song in song_database[db_key][album]:
                    print("\t\tSong:", song)
    elif cmd == "2":
        print("Artists:")
        for artist in song_database.keys():
            print(f"\t{artist}")
    elif cmd == "3":
        print("Artists/Albums:")
        for artist in song_database.keys():
            for album in song_database[artist].keys():
                print(f"{artist}/{album}")
    elif cmd == "4":
        artist = input("Enter artist name: ")
        songs = find_artist_songs(artist, song_database)
        print(f"Artist: {artist} has {len(songs)} songs")
        for song in songs:
            print(f"\t{song}")
    elif cmd == "5":
        songs_text = input("Enter text to search for in song titles: ")
        songs = find_songs_like(songs_text, song_database)
        print(f"Found {len(songs)} including text {songs_text}")
        for song in songs:
            print(song)
    elif cmd == "6":
        print("Adding song to database")
        artist = input("Enter artist name: ")
        album = input("Enter album name: ")
        song = input("Enter song title: ")
        if artist not in song_database:
            song_database[artist] = {album: [song, ], }
        elif album not in song_database[artist]:
            song_database[artist][album] = [song, ]
        elif song not in song_database[artist][album]:
            song_database[artist][album].append(song)
        else:
            print(f"{artist}/{album}/{song} already in database, not added")
        print(f"{artist}/{album}/{song} added to database")
    elif cmd == "7":
        print("Removing song from database")
        artist = input("Enter artist name: ")
        album = input("Enter album name: ")
        song = input("Enter song title: ")
        if artist in song_database and album in song_database[artist] and song in song_database[artist][album]:
            song_database[artist][album].remove(song)
            # These are extra steps that the students may not consider, removing empty values
            if len(song_database[artist][album]) == 0:
                song_database[artist].pop(album)
            if len(song_database[artist].keys()) == 0:
                song_database.pop(artist)
            print(f"{artist}/{album}/{song} removed from the database")
    elif cmd == "8":
        backup_db(song_database)
        print("Database backed up")
    elif cmd == "9":
        song_database = restore_db()
        print("Database restored")
    elif cmd == "h":
        help()
    else:
        print("You entered an invalid command")
